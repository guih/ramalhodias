<?php get_header();
	$term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );

	$image = '';
	if( get_query_var('term') === 'wedding-filmes' ){
		$image = '/assets/images/ptbg6-2000x509-75.jpg';
	}
	if( get_query_var('term') === 'kids-filmes' ){
		$image = '/assets/images/10974346-429657343866378-723263234175141765-o-2000x1338-82-2000x1338-60.jpg';
	}
	if( get_query_var('term') === 'fifteens' ){
		$image = '/assets/images/886892-571894946309283-5769796101349336316-o-2000x1335-9-2000x1335-69.jpg';
	}
?>
<section class="mbr-section article mbr-after-navbar" id="Casamento-msg-box8-0" style="background-image: url(<?php echo get_template_directory_uri() . $image; ?>); padding-top: 160px; padding-bottom: 120px;">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2">
                	Filmes <?php echo RamalhoDias::get_event_label($term->name); ?>
                </h3>
                
                
            </div>
        </div>
    </div>

</section>
<?php 
global $wp_query;
foreach ($wp_query->posts as $key => $post) {
	$video_url  = get_post_meta( $post->ID, '_video_url', true );
	$video_type = get_post_meta( $post->ID, '_video_type', true );
?>
<section class="mbr-section" id="Casamento-msg-box4-0" style="background-color: rgb(242, 242, 242); padding-top: 80px; padding-bottom: 80px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-7 image-size" style="width: 50%;">
                  <div class="mbr-figure">
                 <?php 

                 	echo wp_oembed_get( $video_url ); 


                 ?>
                  </div>
              </div>

              


              <div class="mbr-table-cell col-md-5 text-xs-center text-md-left content-size">
                  <h3 class="mbr-section-title display-2">
                  	<a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
                  

                  
              </div>


              

            </div>
        </div>
    </div>

</section>
<?php } ?>


<?php get_footer(); ?>