<?php 

class RamalhoDias{
	public function __construct(){
		$this->registerHooks();
	}

	public function registerHooks(){
		add_theme_support('thumbnails');
		// CPTS Tax
		add_action( 'init', array( &$this, 'register_post_types' ) );
		add_action( 'init', array( &$this, 'register_taxonomies' ) );

		// on theme changed
		add_action( 'after_setup_theme', array( &$this, 'on_setup_theme' ) );

		// menus
		add_action('admin_menu', array( &$this, 'theme_settings_view' ) );
		add_action('admin_init', array( &$this, 'register_settings' ) );
		
		//
		add_action( 'init', array( &$this, 'register_menus' ) );
		add_action( 'init', array( &$this, 'create_image_sizes' ) );	

		add_action( 'add_meta_boxes',  array( &$this, 'add_events_metaboxes' ) );
		add_action('save_post', array( &$this, 'wpt_save_events_meta' ), 1, 2); // save the custom fields
		add_action('save_post', array( &$this, 'save_gallery' ), 1, 2); // save the custom fields
add_action( 'admin_enqueue_scripts',array( $this, 'meta_box_scripts' ) );
	
	}
	function meta_box_scripts() {

	    global $post;

	    wp_enqueue_media( array( 
	        'post' => $post->ID, 
	    ) );

	}	
	function add_events_metaboxes(){
		add_meta_box(
			'metabox_video', 
			'Detalhes do vídeo', 
			array( &$this, 'metabox_video' ), 
			'movie', 
			'normal', 
			'high'
		);
		add_meta_box(
			'metabox_gallery', 
			'Imagens', 
			array( &$this, 'metabox_gallery' ), 
			'featured', 
			'normal', 
			'high'
		);

	}

	function metabox_gallery(){
		global $post;
		
		$images = get_post_meta($post->ID, '_gallery_images', true);
		$current = get_option( '_current_home_gallery' );
		// $images = unserialize( $images );

		// var_dump($images, get_post_meta($post->ID, '_gallery_images', true));
		?>
			<script type="text/javascript">
				window.gallery = <?php echo json_encode($images) !== 'null' ? json_encode($images ): '[]'; ?>;
				
			</script>
			<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/theme/css/app.css">
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/theme/js/app.js"></script>
			<input type="hidden" class="media-input" name="_gallery_images" value="" />
			<button class="media-button" style="display:none">Select image</button>

			<div class="images-featured">
				<div class="add-new image-row">
					<div class="plus">+</div>
					<p>Adicionar imagem...</p>
				</div>
	
			</div>
			<hr>
			<div class="current-featured">
				<input type="checkbox" <?php checked( $current, $post->ID ); ?> name="_current_post"> Destaque da home
				<p><strong>Nota</strong> Ao selecionar a caixa acima você automaticamente desativa o destaque que estava definido anteiormente.</p>
			</div>
		<?php

	}
	function metabox_video() {
		global $post;
		
		$location = get_post_meta($post->ID, '_video_url', true);
		$type = get_post_meta($post->ID, '_video_type', true);
		
		// Echo out the field
		?>	
			<script>
			    jQuery(document).ready(function ($) {
			        //taxonomy
			        var tx = 'movies';

			        var $scope = $('#movie-type-all > ul');
			        $('#publish').click(function(){
			            if ($scope.find('input:checked').length === 0) {
							alert('Selecione uma categoria!');
							return false;
						}
			        });
			    });				
			</script>
			<p>Selecione a profundidade dimensional do vídeo</p>
			<input type="radio" name="_video_type" <?php checked( $type, '2d' ); ?> value="2d"> 2D <br>
			<input type="radio" name="_video_type" <?php checked( $type, '3d' ); ?> value="3d"> 3D
			<hr>
			<p>Url do vídeo(Youtube, Vimeo, Dropbox)</p>
		<?php
		echo '<input type="text" name="_video_url" value="' . $location  . '" class="widefat" />';

	}

	function save_gallery($post_id, $post) {


		$post_data = $_POST;

		if( isset( $_POST ) ){

			if( isset( $post_data['post_type'] )  && $post_data['post_type'] == 'featured' ){

				if( isset($post_data['_gallery_images']) ) {		

					$images = stripslashes( $post_data['_gallery_images'] );

					$images = json_decode($images);
					update_post_meta( $post_data['ID'], '_gallery_images', $images );
				}

				if( isset($post_data['_current_post']) ){
					update_option( '_current_home_gallery', $post_data['ID'] );
				}
			}
		}

	}

	function wpt_save_events_meta($post_id, $post) {

		$post_data = $_POST;

		if( isset( $_POST ) ){

			if( isset( $post_data['post_type'] )  && $post_data['post_type'] == 'movie' ){
				update_post_meta( $post_data['ID'], '_video_url', $post_data['_video_url'] );
				update_post_meta( $post_data['ID'], '_video_type', $post_data['_video_type'] );
			}
		}

	}



	public function register_taxonomies() {
		register_taxonomy(
			'type',
			'photo',
			array(
				'label' => __( 'Tipo' ),
				'rewrite' => array( 'slug' => 'type' ),
				'hierarchical' => true,
			)
		);
		register_taxonomy(
			'movie-type',
			'movie',
			array(
				'label' => __( 'Tipo' ),
				'rewrite' => array( 'slug' => 'movie-type' ),
				'hierarchical' => true,
				// 'labels' => array( 'add_new_item' => 'TEXT' )
			)
		);
	}
	public function register_post_types() {
	    register_post_type( 'featured', array(
	      'public' => true,
	      'has_archive' => 0,
	      'label'  => 'Destaque',
	      'menu_icon' => 'dashicons-star-filled',
	      'supports' => array('title')

	    ) );
	    register_post_type( 'photo', array(
	      'public' => true,
	      'label'  => 'Albuns',
	      'menu_icon' => 'dashicons-format-image',
	      'supports' => array('thumbnail', 'title', 'editor', 'post-formats')

	    ) );
	    register_post_type( 'movie', array(
	      'public' => true,
	      'label'  => 'Filmes',
	      'menu_icon' => 'dashicons-format-video',
	      'supports' => array('thumbnail', 'title'),
	      'labels' => array(
				'name'               => 'Filmes',
				'singular_name'      => 'Filme',
				'menu_name'          => 'Filmes',
				'name_admin_bar'     => 'Filme',
				'add_new'            => 'Adicionar filme',
				'add_new_item'       => 'Adicionar novo filme',
				'new_item'           => 'Novo filme',
				'edit_item'          => 'Editar filme',
				'view_item'          => 'Ver',
				'all_items'          => 'Filmes',
			)
	    ) );
	}
	public function on_setup_theme() {
		add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
		add_theme_support( 'post-thumbnails',  array('post', 'photo', 'movie') );

		add_theme_support('title-tag');
	}
	public function theme_settings_view() {
		add_menu_page(
			'Tema Ramalho dias', 
			'Ramalho Dias', 
			'administrator', 
			'ramalhodias', 
			'settings_view' , 
			'' 
		);	
	}


	public function register_settings() {
		register_setting( 'ramalhodias_options', 'ramalhodias_home_video' );
		register_setting( 'ramalhodias_options', 'facebook_link' );
		register_setting( 'ramalhodias_options', 'vimeo' );
		register_setting( 'ramalhodias_options', 'youtube' );
		register_setting( 'ramalhodias_options', 'gplus' );
		register_setting( 'ramalhodias_options', 'cpt_images' );

	}

	public function register_menus() {
		register_nav_menu( 'ramalhodias-menu', __( 'Menu principal' ) );
	}

	public function create_image_sizes(){
		add_theme_support( 'post-thumbnails',  array('post', 'photo', 'movie') );
		add_image_size( 'home-featured', 800, 536 );
		add_image_size( 'gallery-thumb', 305, 205, true );
	}
	public static function get_event_label($term){
		switch( $term ){
			case '360º':
				$after_text = 'em panorama';
			break;
	        case 'Wedding':
	        case 'Wedding filmes':
	            $after_text = "de Casamentos";
	        break;
	        case 'Kids':
	        case 'Kids Filmes':
	            $after_text = "de Crianças";
	        break;
	        case 'Fifteens':
	            $after_text = "de 15 anos";
	        break;
	        default:
	            $after_text = "";
	        break;
	    }
	    return $after_text;
	}
	
}

?>