<?php 
get_header(); 
	while ( have_posts() ) : the_post();
?>
<section class="engine"></section>
<section 
    class="mbr-section article mbr-after-navbar" 
    id="sobre_nos-msg-box8-0" 
    style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">

    <div class="mbr-overlay">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2"><?php the_title(); ?></h3>
                <div class="lead"><p><?php the_excerpt(); ?></p></div>
                <div><a class="btn btn-success" href="">FAÇA UM TOUR</a></div>
            </div>
        </div>
    </div>

</section>

<section class="mbr-section mbr-section__container article page-content" id="sobre_nos-header3-0">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
               <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<?php 
endwhile;
get_footer(); ?>