<?php 

require "lib/ramalho-dias.class.php";

new RamalhoDias();


function settings_view() {
?>
	<style>
		.ramalhodias-settings h3 {
		    /* color: red; */
		    border-bottom: 1px solid #e0e0e0;
		    padding: .5em 0;
		    font-weight: 100;
		}

		form.ramalhodias-settings {
		    width: 50%;
		}
	</style>
	<div class="wrap">
	<h1>Tema Ramalho Dias</h1>
	<hr class="wp-header-end">
	<form method="post" action="options.php" class="ramalhodias-settings">
	    <?php settings_fields( 'ramalhodias_options' ); ?>
	    <?php do_settings_sections( 'ramalhodias_options' ); ?>
		<h3>Configuração geral</h3>

	    <table class="form-table">
	        <tr valign="top">
	        <th scope="row">Vídeo destaque home</th>
	        <td><input type="text" class="widefat" name="ramalhodias_home_video" value="<?php echo esc_attr( get_option('ramalhodias_home_video') ); ?>" /></td>
	        </tr>      
	        
	    </table>
		<h3>Redes sociais</h3>

	    <table class="form-table">

	    	<tr valign="top">

	        <th scope="row">Facebook</th>
	        <td><input type="text" class="widefat" name="facebook_link" value="<?php echo esc_attr( get_option('facebook_link') ); ?>" /></td>
	        </tr>
	        
	        <tr valign="top">
	        <th scope="row">Vimeo</th>
	        <td><input type="text" class="widefat" name="vimeo" value="<?php echo esc_attr( get_option('vimeo') ); ?>" /></td>
	        </tr>


	        	        <tr valign="top">
	        <th scope="row">G+</th>
	        <td><input type="text" class="widefat" name="gplus" value="<?php echo esc_attr( get_option('gplus') ); ?>" /></td>
	        </tr>
	        	        <tr valign="top">
	        <th scope="row">Youtube</th>
	        <td><input type="text" class="widefat" name="youtube" value="<?php echo esc_attr( get_option('youtube') ); ?>" /></td>
	        </tr>
	    </table>

	    
	    <?php submit_button(); ?>

	</form>
	</div>
<?php 
} 


