
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo get_site_url(); ?>/assets/images/logo-128x128-56.png" type="image/x-icon">
  <meta name="description" content="Photografia &amp; Filmagens">
  <?php wp_head(); ?>
  <title> <?php wp_title('', true,''); ?></title>
<!--   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
 -->
  <link href="https://fonts.googleapis.com/css?family=Architects+Daughter|Raleway|Varela+Round" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-material-design-font/css/material.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/et-line-font-plugin/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/animate.css/animate.min.css">
  <link rel="stylesheet" href="http://flimshaw.github.io/Valiant360/build/css/valiant360.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dropdown/css/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/socicon/css/socicon.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/theme/css/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/mobirise-gallery/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/mobirise/css/mbr-additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css">
  
  
</head>
<body <?php body_class(); ?>>
<div class="hero">
  
  <section id="index-menu-0">

      <nav class="navbar navbar-dropdown bg-color transparent navbar-fixed-top">
          <div class="container">

              <div class="mbr-table heading-menu" style="display:none">
                  <div class="mbr-table-cell">

                      <div class="navbar-brand">
                          <span class="navbar-logo">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-128x128-56.png" alt="RamalhoDias" title="Ramalho Dias">
                          </span>
                          <a class="navbar-caption" href="<?php echo get_site_url(); ?>">Ramalho Dias</a>
                      </div>

                  </div>
                  <div class="mbr-table-cell">

                      <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                          <div class="hamburger-icon"></div>
                      </button>
                      <ul class="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar">
                          <li class="nav-item"><a class="nav-link link" href="<?php echo get_site_url(); ?>">HOME</a></li>
                          <li class="nav-item"><a class="nav-link link" href="<?php echo get_permalink( get_page_by_path( 'sobre-nos' ) ); ?>">SOBRE-NÓS</a></li>
                          <li class="nav-item"><a class="nav-link link" href="<?php echo get_permalink( get_page_by_path( 'fotos' ) ); ?>">FOTOS</a></li>
                          <li class="nav-item dropdown open">
                              <a class="nav-link link dropdown-toggle" href="Filmes.html" data-toggle="dropdown-submenu" aria-expanded="true">FILMES</a>
                              <div class="dropdown-menu">
                                <?php 
                                  $terms = get_terms( 'movie-type', array( 'hide_empty' => false ) );
                                  
                                  foreach ( $terms as $term ) {
                                    $term_link = get_term_link( $term );
                                    if ( is_wp_error( $term_link ) ) continue;
                                    echo '<a class="dropdown-item" href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';                      
                                  }

                                ?>
                              </div>
                          </li>
                          <li class="nav-item"><a class="nav-link link" href="contato.html" aria-expanded="false">CONTATO</a></li>
                      </ul>

                      <button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                          <div class="close-icon"></div>
                      </button>

                  </div>
              </div>

          </div>
      </nav>

  </section>
