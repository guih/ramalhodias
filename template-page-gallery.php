<?php
/*
Template Name: GAlerry
*/
get_header();
?>
<section class="mbr-section article mbr-after-navbar" id="Galeria-msg-box8-0" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/para1-2000x1511-84.jpg); padding-top: 160px; padding-bottom: 120px;">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2">GALERIA DE FOTOS</h3>
                <div class="lead"><p>Certos momentos ficam para sempre.</p></div>
                
            </div>
        </div>
    </div>

</section>

<section class="mbr-gallery mbr-section mbr-section-nopadding mbr-slider-carousel" id="Galeria-gallery4-0" data-filter="true" style="padding-top: 0rem; padding-bottom: 0rem;">
    <!-- Filter -->
    <div class="mbr-gallery-filter container gallery-filter-active">
        <ul>
            <li class="mbr-gallery-filter-all active">Todos</li>
        </ul>
    </div>

    <!-- Gallery -->
    <div class="mbr-gallery-row">
        <div class=" mbr-gallery-layout-default">
            <div>
                <div>
                <?php 
			        $photos = new WP_Query(array(
						'post_type'      => 'photo', 
						'posts_per_page' => 20,  
						'orderby'        => 'rand'
			        ));
                    $ii = 0;

			        while( $photos->have_posts()) : $photos->the_post();
			            $terms = get_the_terms( $post->ID , 'type' );
                 ?>
                    <div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="<?php echo $terms[0]->name ;?>" data-video-url="false">
                        <div href="#lb-Galeria-gallery4-0" data-slide-to="<?php echo $ii; ?>" data-toggle="modal">
                            
                            
                     		<img alt="" src="<?php the_post_thumbnail_url('home-featured'); ?> ">
                     		<span class="icon-focus"></span>   
                            <!-- <div class="title-photo"><?php the_title(); ?></div> -->
                        </div>
                    </div>
                <?php                     
                    $ii++;

                	endwhile; 
                ?>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Lightbox -->
    <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-Galeria-gallery4-0">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <ol class="carousel-indicators">
                 
                    </ol>
                    <div class="carousel-inner">
                             <?php 
                                $i = 0;
                                while( $photos->have_posts()) : $photos->the_post();
                                    $terms = get_the_terms( $post->ID , 'type' );
                            ?>

                                    <div class="carousel-item <?php echo $i ==0 ? 'active' : ''; ?>">
                                        <a href="<?php echo get_permalink(); ?>" class="">
                                            <img alt="" src="<?php the_post_thumbnail_url('home-featured'); ?>">
                                        </a>
	                    			                                   
                                    </div>
                            <?php 
                            $i++;
                                endwhile; 
                            ?>
                    </div>
                    <a class="left carousel-control" role="button" data-slide="prev" href="#lb-Galeria-gallery4-0">
                        <span class="icon-prev" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" role="button" data-slide="next" href="#lb-Galeria-gallery4-0">
                        <span class="icon-next" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                    <a class="close" href="#" role="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>
