<?php 

get_header(); 
	while ( have_posts() ) : the_post();

		$video_url  = get_post_meta( get_the_ID(), '_video_url', true );
		$video_type = get_post_meta( get_the_ID(), '_video_type', true );
?>
<section class="mbr-section article mbr-after-navbar" id="sobre_nos-msg-box8-0" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>); padding-top: 160px; padding-bottom: 120px;">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2"><?php the_title(); ?></h3>
                <div class="lead"><p><?php echo wp_trim_words( get_the_content(), 30);?></p></div>
                <!-- <div><a class="btn btn-success" href="<?php echo get_permalink( 'movies' ); ?>">Mais vídeos</a></div> -->
            </div>
        </div>
    </div>

</section>
<section class="video-yeld">
	 <?php
	 	if(  $video_type === '3d' ):
	  		echo '<div class="valiant" data-video-src="'.$video_url.'"></div>'; 
	  		?>
				<script>
					window.onload = function(){
						jQuery('.valiant').Valiant360();
					}
				</script>
	  		<?php
		endif;
	 	if(  $video_type === '2d' ):
	  		echo wp_oembed_get( $video_url ); 
		endif;
	?>
</section>
<?php
endwhile;
get_footer();
 ?>
