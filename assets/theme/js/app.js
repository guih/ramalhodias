;(function($){
	if( window.gallery.length == 0  || !window.gallery)	
		window.gallery = [];

		setInterval(function(){			
			var attr = $('.modal-dialog').attr('style');

			if (typeof attr !== typeof undefined && attr !== false) {
				$('.modal-dialog').removeAttr('style');
			}
		},10);
	
	function setData(data){
		console.log('MyDarta', data)

		$('.images-featured').data('meta', data);
		$('.media-input').val(data);
	}
	var onWindowload = function(selector, button_selector)  {
		var dialog = "";

		window.gallery.filter(function(e){return e}).forEach(function(element, index){
			dialog = ['<div class="image-row" data-index="'+index+'">',
				'<img src="'+element.url+'">',
				'<div class="remove">remover</div>',
				'</div>'
			].join('');		

			$('.add-new').before(dialog)

		});
		var clicked_button = false;
		$(selector).each(function (i, input) {
			var button = $('.add-new');
			button.click(function (event) {
				event.preventDefault();
				var selected_img;
				clicked_button = $(this);

				if(wp.media.frames.ramalhodias) {
					wp.media.frames.ramalhodias.open();
					return;	
				}


				wp.media.frames.ramalhodias = wp.media({
					title: 'Selecione as fotos da galeria',
					multiple: true,
					library: {
						type: 'image'
					},
					button: {
						text: 'Inserir como destaque'
					}
				});

				var onSelectImage = function() {
					var selection = wp.media.frames.ramalhodias.state().get('selection');
					if (!selection) {
						return;
					}


					selection.forEach(function(attachment, index) {
						var url = attachment.attributes.sizes.thumbnail.url;
						var large = attachment.attributes.sizes.full.url;
						console.log(attachment);

						var dialog = ['<div class="image-row" data-index="'+$('.image-row:not(.add-new)').length+'">',
							'<img src="'+url+'">',
							'<div class="remove">remover</div>',
							'</div>'
						].join('');

						window.gallery.push({
							url 	: url,
							full: large,
							index 	: index,
							id 		: attachment.attributes.id
						});

						setData( JSON.stringify( window.gallery ) );

						$('.add-new').before(dialog)
					});
				};


				wp.media.frames.ramalhodias.on('select', onSelectImage);
				wp.media.frames.ramalhodias.open();
			});
		});
	};  
	$(document).ready(function(){
		$('body').on('click', '.remove', function() {
			var $el = $(this);
			var $index = $(this).parent().data('index');

			var $data =  window.gallery.filter(function(e){return e});

			 $data.splice($index, 1);

			$('.image-row:not(.add-new)').remove()
			setData( JSON.stringify( $data ) );

			window.gallery = $data;
			$data.forEach(function(element, index){
				dialog = ['<div class="image-row" data-index="'+($('.image-row:not(.add-new)').length)+'">',
					'<img src="'+element.url+'">',
					'<div class="remove">remover</div>',
					'</div>'
				].join('');		

				$('.add-new').before(dialog)

			});
			$(this).parent().fadeOut().remove();

		});
		onWindowload('.media-input', '.media-button');
		setData( JSON.stringify( window.gallery ) );
	})
	console.log('tiggered!')
})(jQuery)	
