<?php 
get_header(); 
	while ( have_posts() ) : the_post();
?>
<section class="mbr-section article mbr-after-navbar" id="sobre_nos-msg-box8-0" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>); padding-top: 160px; padding-bottom: 120px;">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2"><?php the_title(); ?></h3>
                <div class="lead"><p><?php echo wp_trim_words( get_the_content(), 30);?></p></div>
                <div><a class="btn btn-success" href="<?php echo get_permalink( get_page_by_path( 'fotos' ) ); ?>">FAÇA UM TOUR</a></div>
            </div>
        </div>
    </div>

</section>

<section class="mbr-gallery mbr-section mbr-section-nopadding mbr-slider-carousel" id="Galeria-gallery4-0" data-filter="true" style="padding-top: 0rem; padding-bottom: 0rem;">
	<?php
				$attachments = get_posts(array(
					'post_type'   => 'attachment',
					'numberposts' => -1,
					'post_status' => null,
					'post_parent' => get_the_ID(),
					'order'       => 'ASC',
					'orderby'     => 'id',
                ));
				$largeImages = array();
				$tinyImages = array();
				unset( $attachments[0] );
				foreach( $attachments as $image ) :
					$largeImages[]= wp_get_attachment_image( 
						$image->ID, 
						'full', 
						"", 
						array( 
							"class" => "img-responsive"
						) 
					); 
					$tinyImages[]= wp_get_attachment_image( 
						$image->ID, 
						'large', 
						"", 
						array( 
							"class" => "img-responsive"
						) 
					);  
				endforeach;

		preg_match_all( '/<img .*?(?=src)src=\"([^\"]+)\"/si', implode( '', $largeImages), $allpics );
		preg_match_all( '/<img .*?(?=src)src=\"([^\"]+)\"/si', implode( '', $tinyImages ), $tinyImages );
		$i = 0;
	?>
    <!-- Gallery -->
    <div class="mbr-gallery-row">
        <div class=" mbr-gallery-layout-default">

            	<?php foreach($tinyImages[1] as $picture) : ?>
            <div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Casamentos" data-video-url="false">
                <div href="#lb-Galeria-gallery4-0" data-slide-to="<?php echo $i; ?>" data-toggle="modal">
                    
                    

                    <img alt="" src="<?php echo $picture; ?>">
                    
                    <span class="icon-focus"></span>
                        
				</div>
			</div>
			<?php $i++;endforeach; ?>


		</div>
	</div>
 <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-Galeria-gallery4-0">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <ol class="carousel-indicators">
                    	<?php 
	                    	$key = 0;
	                    	foreach($allpics[1] as $picture) : 
                    	?>
						<li 
							data-app-prevent-settings="" 
							class="<?php echo $key === 0 ? 'active' : ''; ?>"
							data-target="#lb-Galeria-gallery4-0" 
							data-slide-to="<?php echo $key; ?>"></li>
					
						<?php 
							$key++;
							endforeach;
						?>
                    </ol>

                    <div class="carousel-inner">
                        <?php 
	                    	$key = 0;
	                    	foreach($allpics[1] as $picture) : 
                    	?>
	                        <div class="carousel-item <?php echo $key === 0 ? 'active' : ''; ?>">
	                            <img alt="" src="<?php echo $picture; ?>">
	                        </div>
                   		<?php 
							$key++;
							endforeach;
						?>
                    </div>
                    <a class="left carousel-control" role="button" data-slide="prev" href="#lb-Galeria-gallery4-0">
                        <span class="icon-prev" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" role="button" data-slide="next" href="#lb-Galeria-gallery4-0">
                        <span class="icon-next" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                    <a class="close" href="#" role="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 
endwhile;
get_footer(); ?>