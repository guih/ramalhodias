<footer class="mbr-small-footer mbr-section mbr-section-nopadding" id="footer1-2">
    
    <div class="container">
        <p class="text-xs-center">
          Ramalho Dias &copy;  <?php echo date('Y'); ?> -  Todos os direitos reservados
        </p>
    </div>
</footer>


  <script src="<?php echo get_template_directory_uri(); ?>/assets/web/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/tether/tether.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/smooth-scroll/SmoothScroll.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/viewportChecker/jquery.viewportchecker.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/dropdown/js/script.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/touchSwipe/jquery.touchSwipe.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/jquery-mb-ytplayer/jquery.mb.YTPlayer.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/masonry/masonry.pkgd.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/jarallax/jarallax.js"></script>
  <script type="text/javascript" src="http://cdn.jssor.com/script/jssor.slider-22.2.8.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/theme/js/script.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/mobirise-gallery/player.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/mobirise-gallery/script.js"></script>
  <script src="http://flimshaw.github.io/Valiant360/js/three.min.js"></script>
  <script src="http://flimshaw.github.io/Valiant360/build/jquery.valiant360.js"></script>  
  <script type="text/javascript">
  </script>
  <?php wp_footer(); ?>
  </body>
</html>