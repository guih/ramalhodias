<section class="mbr-section article what-we-do" id="index-msg-box3-1" >
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2">O que nós fazemos</h3>
                <div class="lead"><p>Os casamentos são eventos significativos em nossa vida,  realizar sonhos em filmes, realidade em conquista, emoções e apresendizagem.</p></div>
                <div><a class="btn btn-secondary" href="<?php echo get_permalink( get_page_by_path( 'sobre-nos' ) ); ?>">SOBRE-NÓS</a></div>
            </div>
        </div>
    </div>
</section>
<section class="mbr-cards mbr-section mbr-section-nopadding" id="index-features4-0" style="background-color: rgb(255, 255, 255);">


    <?php 
        $terms = get_terms( 'movie-type', array(
            'hide_empty' => false,
        ) );
    ?>
    <div class="mbr-cards-row row">
        <?php foreach ((object)$terms as $key => $term): ?>
            <?php 
                $post = array();
                if( $term->slug  === 'kids-filmes' ) :
                    $post['icon']           = 'icon-happy';
                    $post['caption-bottom'] = 'Filmes infantil';              
                endif;
                if( $term->slug  === 'fifteens' ) :
                    $post['icon']           = 'icon-profile-female';
                    $post['caption-bottom'] = 'Filmes de 15 anos'; 
                endif;

                if( $term->slug  === 'wedding-filmes' ) :
                    $post['icon']           = 'icon-video';
                    $post['caption-bottom'] = 'Filmes de casamanentos'; 
                endif;

                $post['url']         = get_term_link( $term->term_id );                
                $post['caption-top'] = $term->name;
                if( !isset( $post['icon'] ) ) continue;
            ?>

            <div class="mbr-cards-col col-xs-12 col-lg-4" style="padding-top: 80px; padding-bottom: 80px;">
                <div class="container">
                    <div class="card cart-block">
                        <div class="card-img iconbox"><a href="<?php echo $post['url']; ?>" class="etl-icon <?php echo $post['icon']; ?> mbr-iconfont mbr-iconfont-features4" style="color: black;"></a></div>
                        <div class="card-block">
                            <h4 class="card-title"><?php echo $post['caption-top']; ?></h4>
                            <h5 class="card-subtitle"><?php echo $post['caption-bottom']; ?></h5>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</section>