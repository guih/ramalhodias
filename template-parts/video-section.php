<div class="heading">
<?php 
$video_url = esc_attr( get_option('ramalhodias_home_video') );
$photo_galery = esc_attr( get_option('_current_home_gallery') );
    
?>


<?php if( $video_url !== "" ) : ?>

    <script type="text/javascript">var video_url = true;</script>

    

<section class="mbr-section mbr-section-hero mbr-section-full mbr-section-with-arrow mbr-after-navbar" id="index-header4-0" data-bg-video="<?php echo $video_url; ?>">

    <div class="mbr-overlay"></div>

    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="mbr-section col-md-10 col-md-offset-2 text-xs-right">
                </div>
            </div>
        </div>
    </div>

    <div class="mbr-arrow mbr-arrow-floating" aria-hidden="true"><a href="#index-msg-box3-1"><i class="mbr-arrow-icon"></i></a></div>

</section>
<?php endif; ?>
<div style="position: relative; width: 100%; overflow: hidden;">
    <div style="position: relative; margin: 0 auto; text-align: center; ">
<div class="slider-holder">

<?php if( ( $video_url == "" || !$video_url ) && $photo_galery && sizeof(get_post_meta($photo_galery, '_gallery_images', true)) >0) : ?>
<div id="main-slider" style="transform: scale(1.2);display:none;position: relative; top: 0px; left: 0px; width: 600px; height: 300px;">
    <!-- Slides Container -->
    <div u="slides" style="cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width: 600px; height: 300px;">
    <?php 
    $attachments =  get_post_meta($photo_galery, '_gallery_images', true);


    foreach( $attachments as $image ) :
        $largeImages[]= wp_get_attachment_image( 
            $image->id, 
            'full', 
            "", 
            array( 
                "class" => ""
            ) 
        ); 
    endforeach;

    preg_match_all( '/<img .*?(?=src)src=\"([^\"]+)\"/si', implode( '', $largeImages), $allpics );
    foreach ($largeImages as $picture) {
        ?><div> <?php
        echo str_replace( '/>', 'u="image" />', $picture);
        ?></div> <?php

    }
?>
<div id="progress-slider" u="progress" style="position: absolute; left: 0; bottom: 0; width: 0%; height: 5px; background-color: rgba(255,255,255,0.5); z-index: 100;"></div>
    </div>
</div>
    </div>
</div>
<!-- <div class="cover"></div> -->
<div class="mbr-arrow mbr-arrow-floating" aria-hidden="true"><a href="#index-msg-box3-1"><i class="mbr-arrow-icon"></i></a></div>

<?php endif; ?>
</div>

</div>