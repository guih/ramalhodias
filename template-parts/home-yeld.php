<div class="site-load" style="position: fixed;z-index: 99999999999999; width: 100%; height: 100%; text-align: center; text-transform: uppercase; color: rgb(255, 255, 255); padding-top: 18em;/* display: none; */ background: rgb(47, 47, 47);top: 0;">Carregando...</div>
<?php require 'video-section.php'; ?>

<?php require 'what-we-do-section.php'; ?>

<section class="mbr-section article" id="index-msg-box3-0" style="background-color: rgb(242, 242, 242); padding-top: 120px; padding-bottom: 120px;">

    
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2">Galeria de fotos</h3>
                <div class="lead"><p>&nbsp; &nbsp;Certos momentos ficam para sempre!</p></div>
                <div><a class="btn btn-secondary" href="<?php echo get_permalink( get_page_by_path( 'fotos' ) ); ?>">VEJA MAIS FOTOS</a></div>
            </div>
        </div>
    </div>

</section>

<section class="mbr-gallery mbr-section mbr-section-nopadding mbr-slider-carousel" id="index-gallery1-0" data-filter="false" style="background-color: rgb(255, 255, 255); padding-top: 0rem; padding-bottom: 0rem;">  <!-- Filter -->    

    <!-- Gallery -->
    <div class="mbr-gallery-row">
        <div class=" mbr-gallery-layout-default">
            <div>
                <div>
    <?php 
        $i = 0;
        $photos = new WP_Query(array(
            'post_type' => 'photo', 
            'posts_per_page' => 4,  
            'orderby'        => 'rand'
        ));

        while( $photos->have_posts()) : $photos->the_post();
            $terms = get_the_terms( $post->ID , 'type' );

    ?>

            <div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Responsive" data-video-url="false">
                <div href="#lb-index-gallery1-0" data-slide-to="<?php echo $i; ?>" data-toggle="modal">
                     
                    <img alt="" src="<?php the_post_thumbnail_url('home-featured'); ?> ">              
                    <span class="icon-focus"></span>
                    <span class="mbr-gallery-title"><?php echo $terms[0]->name; ?> - <?php echo RamalhoDias::get_event_label( $terms[0]->name ); ?></span>
                </div>
            </div>
    <?php 
        $i++;
        endwhile; 
    ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Lightbox -->
    <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-index-gallery1-0">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <ol class="carousel-indicators">
                        <li data-app-prevent-settings="" data-target="#lb-index-gallery1-0" data-slide-to="0"></li>
                        <li data-app-prevent-settings="" data-target="#lb-index-gallery1-0" data-slide-to="1"></li>
                        <li data-app-prevent-settings="" data-target="#lb-index-gallery1-0" data-slide-to="2"></li>

                        <li data-app-prevent-settings="" data-target="#lb-index-gallery1-0" class="" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                            <?php 
                                $i = 0;
                                while( $photos->have_posts()) : $photos->the_post();
                                    $terms = get_the_terms( $post->ID , 'type' );
                            ?>
                                    <div class="carousel-item <?php echo $i ==0 ? 'active' : ''; ?>">
                                        <img alt="" src="<?php the_post_thumbnail_url('home-featured'); ?>">
                                    </div>
                            <?php 
                            $i++;
                                endwhile; 
                            ?>
                    </div>
                    <a class="left carousel-control" role="button" data-slide="prev" href="#lb-index-gallery1-0">
                        <span class="icon-prev" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" role="button" data-slide="next" href="#lb-index-gallery1-0">
                        <span class="icon-next" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                    <a class="close" href="#" role="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-info mbr-info-extra mbr-section mbr-section-md-padding" id="index-msg-box1-0" style="background-color: rgb(242, 242, 242); padding-top: 60px; padding-bottom: 60px;">

    
    <div class="container">
        <div class="row">


            


            <div class="mbr-table-md-up">
                <div class="mbr-table-cell mbr-right-padding-md-up col-md-8 text-xs-center text-md-left">
                    <h2 class="mbr-info-subtitle mbr-section-subtitle">Entre em contato para agendar sua visita!</h2>
                    
                </div>

                <div class="mbr-table-cell col-md-4">
                    <div class="text-xs-center"><a class="btn btn-primary" href="#">AGENDAR AGORA</a></div>
                </div>
            </div>


        </div>
    </div>
</section>

<section class="mbr-section mbr-section-md-padding mbr-parallax-background" id="index-social-buttons2-0" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/landscape3.jpg); padding-top: 90px; padding-bottom: 90px;z-index: 999;">
    
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2">SIGA-NOS</h3>
                <div> 
                <?php 
                    $facebook = esc_attr( get_option('facebook_link') );
                    $vimeo = esc_attr( get_option('vimeo') );
                    $gplus = esc_attr( get_option('gplus') );
                    $youtube = esc_attr( get_option('youtube') );
                ?>
                        <?php if( $facebook !== '' && $facebook ) : ?>
                            <a class="btn btn-social" title="Facebook" target="_blank" href=" <?php  echo $facebook; ?>">
                                <i class="socicon socicon-facebook"></i>
                            </a> 
                        <?php endif; ?>

                        <?php if( $gplus !== '' && $gplus ) : ?>

                            <a class="btn btn-social" title="Google+" target="_blank" href="<?php  echo $gplus; ?>">
                                <i class="socicon socicon-google"></i>
                            </a> 
                        <?php endif; ?>
                        <?php if( $youtube !== '' && $youtube ) : ?>
                            <a class="btn btn-social" title="YouTube" target="_blank" href="<?php echo $youtube; ?>">
                                <i class="socicon socicon-youtube"></i>
                            </a> 
                        <?php endif; ?>
                        <?php if( $vimeo !== '' && $vimeo ) : ?>
                            <a class="btn btn-social" title="Instagram" target="_blank" href="<?php echo $vimeo; ?>">
                                <span class="socicon socicon-vimeo mbr-iconfont mbr-iconfont-btn"></span>
                            </a>   
                        <?php endif; ?>

                    </div>
            </div>
        </div>
    </div>
</section>